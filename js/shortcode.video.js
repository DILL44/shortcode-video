/* 
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
(function($) {
$('.video-clicable').click(function () {
  var id = this.id.substr(6);
  if ($(this).hasClass('youtube')){
    $(this).html( '<iframe src="https://www.youtube.com/embed/'+id+'?rel=0&autoplay=1"n\
       frameborder="0" allowfullscreen></iframe>');
    if (typeof ga == 'function') {
        ga('send', 'event', 'video', 'lecture', 'idYoutube : '+id);
    }
  }
  else if  ($(this).hasClass('vimeo')){
    $(this).html('<iframe class="{{ class }}" src="https://player.vimeo.com/video/'+id+'?rel=0&autoplay=1"n\
     frameborder="0" allowfullscreen></iframe>');
    if(typeof ga == 'function') {
            ga('send', 'event', 'video', 'lecture', 'idYoutube : '+id);
    }
  }
  return false;
});
}(jQuery));




