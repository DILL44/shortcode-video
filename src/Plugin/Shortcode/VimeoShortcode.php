<?php
/**
 * @file
 * Contains \Drupal\shortcode_video\Plugin\Shortcode\YoutubeShortcode.
 */

namespace Drupal\shortcode_video\Plugin\Shortcode;

use Drupal\Core\Language\Language;
use Drupal\shortcode\Plugin\ShortcodeBase;

/**
 * The youtube shortcode.
 *
 * @Shortcode(
 *   id = "vimeo",
 *   title = @Translation("Vimeo"),
 *   description = @Translation("Insert a vimeo video.")
 * )
 */

class VimeoShortcode extends ShortcodeBase {

  /**
   * {@inheritdoc}
   */
  public function process($attributes, $text, $langcode = Language::LANGCODE_NOT_SPECIFIED) {

    // Merge with default attributes.
    $attributes = $this->getAttributes([
      'id' => '',
      'couv' => '',
      'alt' => '',
      'opt' => '',
    ],
      $attributes
    );

    $class = $this->addClass($attributes['opt'], 'vimeo');

    $output = [
      '#theme'      => 'shortcode_vimeo',
      '#play'       => \Drupal::request()->query->get('play'),
      '#playId'     =>  \Drupal::request()->query->get('play-'.$attributes['id']),
      '#id'         => $attributes['id'],
      '#img'        => $attributes['couv'],
      '#alt'        => $attributes['alt'],
      '#class'      => $class,
    ];
    $renderer = \Drupal::service('renderer');
    return $renderer->render($output);
  }
  
  public function tips($long = FALSE) {
    $output = [];
    $output[] = '<p><strong>' . t('[vimeo id="115220788" (couv="image.png"|alt="alt text"|opt="cadre")/]') . '</strong> ';
    $output[] = t('Inserts vimeo video base on id vimeo.') . '</p>';
    return implode(' ', $output);
  }
}